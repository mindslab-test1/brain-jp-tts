import torch
import argparse
import re

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input',
                    nargs='?',
                    required=True,
                    help='input path.',
                    type=str)
parser.add_argument('-o', '--output',
                    nargs='?',
                    required=True,
                    help='output path.',
                    type=str)

args = parser.parse_args()

checkpoint = torch.load(args.input)

checkpoint['hparams'].lang = "cmu"

cmu_pattern = re.compile(r'^(@.*)([0-2])$')
symbols = []
symbol_idx_list = []
for symbol_idx, symbol in enumerate(checkpoint['hparams'].symbols):
    cmu_match = cmu_pattern.match(symbol)
    if cmu_match:
        new_symbol = cmu_match.group(1)
        if cmu_match.group(2) == '1' and new_symbol in symbols:
            print('\treplace {} with {}'.format(new_symbol, symbol))
            symbol_idx_list[symbols.index(new_symbol)] = symbol_idx
    else:
        new_symbol = symbol
    if new_symbol not in symbols:
        symbols.append(new_symbol)
        symbol_idx_list.append(symbol_idx)
        print('{}. use {}'.format(symbol_idx, symbol))
    else:
        print('{}. skip {}'.format(symbol_idx, symbol))

checkpoint['hparams'].symbols = symbols
checkpoint['hparams'].n_symbols = len(symbols)

old_emb = checkpoint['state_dict']['module.embedding.weight']
new_emb = torch.nn.Embedding(checkpoint['hparams'].n_symbols, 512)

for new_idx, old_idx in enumerate(symbol_idx_list):
    new_emb.weight.data[new_idx] = old_emb[old_idx]

checkpoint['state_dict']['module.embedding.weight'] = new_emb.weight.data

torch.save(checkpoint, args.output)
