import time
import torch
import sys
import subprocess
import os
from argparse import ArgumentParser, REMAINDER


parser = ArgumentParser()
parser.add_argument('-o', '--output_directory', type=str, default="trained",
                    help='directory to save checkpoints')
parser.add_argument('-l', '--log_directory', type=str, default="logs",
                    help='directory to save logs')
parser.add_argument('-d', '--devices', type=int, action='append')
parser.add_argument('training_script', nargs=REMAINDER)
args = parser.parse_args()

argslist = args.training_script
if args.devices is None:
    args.devices = range(torch.cuda.device_count())
num_gpus = len(args.devices)
argslist.append('--n_gpus={}'.format(num_gpus))
workers = []
job_id = time.strftime("%Y_%m_%d-%H%M%S")
argslist.append("--group_name={}".format(job_id))
argslist.append("--output_directory={}".format(args.output_directory))
argslist.append("--log_directory={}".format(os.path.join(args.log_directory, 'tensorboard')))

log_dir = args.log_directory
if not os.path.exists(log_dir):
    os.makedirs(log_dir)

for rank, device in zip(range(num_gpus), args.devices):
    argslist.append('--rank={}'.format(rank))
    argslist.append('--local_rank={}'.format(device))
    stdout = open(os.path.join(log_dir, "rank_{}.out".format(rank)), "w")
    stderr = open(os.path.join(log_dir, "rank_{}.err".format(rank)), "w")
    print(argslist)
    p = subprocess.Popen([str(sys.executable), '-u']+argslist,
                         stdout=stdout, stderr=stderr)
    workers.append(p)
    argslist = argslist[:-2]

for p in workers:
    p.wait()
