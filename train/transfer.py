from core.model import Tacotron2
from core.utils import speaker_mode


def _single_to_multi_speaker(state_dict, key, value):
    dim1, dim2 = state_dict[key].size()
    value.data[:dim1, :dim2] = state_dict[key]
    state_dict[key] = value.data


def load_state_dict(model, checkpoint_dict, speaker_list):
    module = model if type(model) == Tacotron2 else model.module
    state_dict = checkpoint_dict['state_dict']
    state_dict = {
        k.replace('module.', ''): v for k, v in state_dict.items()
    }
    if module.speaker_mode == speaker_mode.ENTITY_EMBEDDING:
        if 'speaker_embedding.weight' not in state_dict:
            print('single to multi')
            state_dict['speaker_embedding.weight'] = module.speaker_embedding.weight.data
            _single_to_multi_speaker(
                state_dict, 'decoder.attention_rnn.weight_ih',
                module.decoder.attention_rnn.weight_ih
            )
            _single_to_multi_speaker(
                state_dict, 'decoder.attention_layer.memory_layer.linear_layer.weight',
                module.decoder.attention_layer.memory_layer.linear_layer.weight
            )
            _single_to_multi_speaker(
                state_dict, 'decoder.decoder_rnn.weight_ih',
                module.decoder.decoder_rnn.weight_ih
            )
            _single_to_multi_speaker(
                state_dict, 'decoder.linear_projection.linear_layer.weight',
                module.decoder.linear_projection.linear_layer.weight
            )
            _single_to_multi_speaker(
                state_dict, 'decoder.gate_layer.linear_layer.weight',
                module.decoder.gate_layer.linear_layer.weight
            )
            if module.mask_embedding is not None:
                state_dict['mask_embedding.weight'] = module.mask_embedding.weight.data
        elif speaker_list != checkpoint_dict['hparams'].speaker:
            print("speaker list is different.")
            emb_name = 'speaker_embedding.weight'
            speaker_dict = {
                speaker: idx for idx, speaker in enumerate(speaker_list)
            }
            for old_idx, speaker in enumerate(checkpoint_dict['hparams'].speaker):
                if speaker in speaker_dict:
                    new_idx = speaker_dict[speaker]
                    old_data = state_dict[emb_name][old_idx]
                    if old_data.type() == 'torch.HalfTensor':
                        old_data = old_data.float()
                    module.speaker_embedding.weight.data[new_idx] = old_data

            state_dict[emb_name] = module.speaker_embedding.weight.data.cpu()
            module.load_state_dict(state_dict)
            for sub_module in [module.embedding, module.encoder, module.stylenet, module.decoder, module.postnet]:
                if sub_module is not None:
                    for param in sub_module.parameters():
                        param.requires_grad = False
            return

    rnn_list = [module.decoder.attention_rnn, module.decoder.decoder_rnn]
    name_list = ['attention_rnn', 'decoder_rnn']
    for rnn, name in zip(rnn_list, name_list):
        weight_name = 'decoder.{}.weight_ih'.format(name)
        if rnn.weight_ih.size() != state_dict[weight_name].size():
            print('size mismatch for {}: model {}, checkpoint {}'.format(
                weight_name,
                rnn.weight_ih.size(),
                state_dict[weight_name].size()
            ))
            state_dict[weight_name].data = rnn.weight_ih.data

    module.load_state_dict(state_dict)


def load_only_decoder_state_dict(model, checkpoint_dict):
    module = model if type(model) == Tacotron2 else model.module
    state_dict = checkpoint_dict['state_dict']
    state_dict = {
        k.replace('module.', ''): v for k, v in state_dict.items()
    }

    print('use foreign language baseline to train model, decoder states loaded.')
    module_state_dict = module.state_dict()
    for k, v in state_dict.items():
        if k.startswith('decoder'):
            module_state_dict[k] = v
    state_dict = module_state_dict
    module.load_state_dict(state_dict)
