import tensorflow as tf

from core.text import set_lang
from core.utils import speaker_mode


def create_hparams(hparams_string=None, verbose=False):
    """Create model hyperparameters. Parse nondefault from given string."""

    hparams = tf.contrib.training.HParams(
        ################################
        # Experiment Parameters        #
        ################################
        epochs=2500,
        iters_per_checkpoint=500,
        seed=1234,
        dynamic_loss_scaling=True,
        fp16_run=False,
        distributed_run=False,
        dist_backend="nccl",
        dist_url="tcp://localhost:54321",
        cudnn_enabled=True,
        cudnn_benchmark=False,

        ################################
        # Data Parameters             #
        ################################
        load_mel_from_disk=False,
        resource_root="/DATA1/J_baseline_22k_new",
        training_files='train_10s.txt', #
        validation_files='val.txt',
        lang='jap',#'eng2',#'cht',#'kor'
        text_cleaners=['japanese_cleaners'],# ['english_cleaners'],#['chinese_cleaners'],#['korean_cleaners'] \#
        # ['japanese_kana_cleaners'] # ['japanese_kana_cleaners']
        sort_by_length=False,
        use_eos=True,

        ################################
        # Audio Parameters             #
        ################################
        max_wav_value=32768.0,
        sampling_rate=22050,
        filter_length=1024,
        hop_length=256,
        win_length=1024,
        n_mel_channels=80,
        mel_fmin=0.0,
        mel_fmax=8000.0,

        ################################
        # Model Parameters             #
        ################################
        symbols=None,
        n_symbols=None,
        symbols_embedding_dim=512,

        # Encoder parameters
        encoder_kernel_size=5,
        encoder_n_convolutions=3,
        encoder_embedding_dim=512,

        # Speaker embedding
        speaker_mode=speaker_mode.ENTITY_EMBEDDING,
        speaker=['jvs001', 'jvs002', 'jvs003', 'jvs004', 'jvs005', 'jvs006', 'jvs007', 'jvs008', 'jvs009', 'jvs010',
                'jvs011', 'jvs012', 'jvs013', 'jvs014', 'jvs015', 'jvs016', 'jvs017', 'jvs018', 'jvs019', 'jvs020',
                'jvs021', 'jvs022', 'jvs023', 'jvs024', 'jvs025', 'jvs026', 'jvs027', 'jvs028', 'jvs029', 'jvs030',
                'jvs031', 'jvs032', 'jvs033', 'jvs034', 'jvs035', 'jvs036', 'jvs037', 'jvs038', 'jvs039', 'jvs040',
                'jvs041', 'jvs042', 'jvs043', 'jvs044', 'jvs045', 'jvs046', 'jvs047', 'jvs048', 'jvs049', 'jvs050',
                'jvs051', 'jvs052', 'jvs053', 'jvs054', 'jvs055', 'jvs056', 'jvs057', 'jvs058', 'jvs059', 'jvs060',
                'jvs061', 'jvs062', 'jvs063', 'jvs064', 'jvs065', 'jvs066', 'jvs067', 'jvs068', 'jvs069', 'jvs070',
                'jvs071', 'jvs072', 'jvs073', 'jvs074', 'jvs075', 'jvs076', 'jvs077', 'jvs078', 'jvs079', 'jvs080',
                'jvs081', 'jvs082', 'jvs083', 'jvs084', 'jvs085', 'jvs086', 'jvs087', 'jvs088', 'jvs089', 'jvs090',
                'jvs091', 'jvs092', 'jvs093', 'jvs094', 'jvs095', 'jvs096', 'jvs097', 'jvs098', 'jvs099', 'jvs100',
                'fujitou_angry', 'fujitou_happy', 'fujitou_normal', 'tsuchiya_angry', 'tsuchiya_happy',
                'tsuchiya_normal', 'uemura_angry', 'uemura_happy', 'uemura_normal',
                'JSUT',
                'toshinori'], #'honoka'
        speaker_embedding_dim=256,
        pretrained_speaker_embedding=None,

        # StyleNet parameters
        use_global_style_tokens=False,
        reference_out_dim=256,
        reference_filters=[64, 64, 128, 128, 256, 256],
        reference_kernel_size=[3, 3],
        reference_stride=[2, 2],

        style_n_tokens=10,
        style_n_heads=4,

        # Decoder parameters
        n_frames_per_step=1,  # currently only 1 is supported
        decoder_rnn_dim=1024,
        prenet_dim=256,
        max_decoder_steps=1000,
        gate_threshold=0.5,
        p_attention_dropout=0.1,
        p_decoder_dropout=0.1,

        # Attention parameters
        attention_rnn_dim=1024,
        attention_dim=128,

        # Location Layer parameters
        attention_location_n_filters=32,
        attention_location_kernel_size=31,

        # Mel-post processing network parameters
        postnet_embedding_dim=512,
        postnet_kernel_size=5,
        postnet_n_convolutions=5,

        ################################
        # Optimization Hyperparameters #
        ################################
        use_saved_learning_rate=False,
        learning_rate=1e-3,
        learning_rate_deacy_rate=0.01,
        start_deacy=50000,
        max_deacy_step=50000,
        weight_decay=1e-6,
        grad_clip_thresh=1.0,
        batch_size=64,
        mask_padding=True,  # set model's padded outputs to padded values
        use_guided_attention_loss=True
    )

    if hparams_string:
        tf.logging.info('Parsing command line hparams: %s', hparams_string)
        hparams.parse(hparams_string)

    if verbose:
        tf.logging.info('Final parsed hparams: %s', hparams.values())

    hparams.symbols = set_lang(hparams.lang, hparams.text_cleaners, hparams.use_eos)
    hparams.n_symbols = len(hparams.symbols)

    return hparams
