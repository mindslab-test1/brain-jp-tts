from torch import nn
import torch

class GuidedAttentionLoss(torch.nn.Module):
    """Wrapper around all loss functions including the loss of Tacotron 2.

    Details:
        - L2 of the prediction before and after the postnet.
        - Cross entropy of the stop tokens
        - Guided attention loss:
            prompt the attention matrix to be nearly diagonal, this is how people usualy read text
            introduced by 'Efficiently Trainable Text-to-Speech System Based on Deep Convolutional Networks with Guided Attention'
    Arguments:
        guided_att_steps -- number of training steps for which the guided attention is enabled
        guided_att_variance -- initial allowed variance of the guided attention (strictness of diagonal)
        guided_att_gamma -- multiplier which is applied to guided_att_variance at every update_states call
    """

    def __init__(self, guided_att_steps, guided_att_variance, guided_att_gamma):
        super(GuidedAttentionLoss, self).__init__()
        self._g = guided_att_variance
        self._gamma = guided_att_gamma
        self._g_steps = guided_att_steps

    def forward(self, alignments, input_lengths, target_lengths, global_step):
        if self._g_steps < global_step: return 0
        self._g = self._gamma ** global_step
        # compute guided attention weights (diagonal matrix with zeros on a 'blurry' diagonal)
        weights = torch.zeros_like(alignments)
        for i, (f, l) in enumerate(zip(target_lengths, input_lengths)):
            grid_f, grid_l = torch.meshgrid(torch.arange(f, dtype=torch.float),
                                            torch.arange(l, dtype=torch.float))
            weights[i, :f, :l] = 1 - torch.exp(-(grid_l / l - grid_f / f) ** 2 / (2 * self._g ** 2))

            # apply weights and compute mean loss
        loss = torch.sum(weights * alignments, dim=(1, 2))
        loss = torch.mean(loss / target_lengths.float())

        return loss


class Tacotron2Loss(nn.Module):
    def __init__(self, use_attn_loss = False):
        super(Tacotron2Loss, self).__init__()
        self.use_attn_loss = use_attn_loss
        if use_attn_loss:
            self.attn_loss = GuidedAttentionLoss(20000, 0.25, 1.00025)

    def forward(self, model_output, targets, inputs, iter):
        mel_target, gate_target = targets[0], targets[1]
        mel_target.requires_grad = False
        gate_target.requires_grad = False
        gate_target = gate_target.view(-1, 1)

        mel_out, mel_out_postnet, gate_out, alignment = model_output
        gate_out = gate_out.view(-1, 1)
        mel_loss = nn.MSELoss()(mel_out, mel_target) + \
            nn.MSELoss()(mel_out_postnet, mel_target)
        gate_loss = nn.BCEWithLogitsLoss()(gate_out, gate_target)
        if self.use_attn_loss:
            alignment_loss = self.attn_loss(alignment, inputs[1], inputs[4], iter)
            return mel_loss + gate_loss + alignment_loss
        else:
            return mel_loss + gate_loss
