#!/usr/bin/env python3.5
# -*- coding: UTF-8 -*-
import grpc
import google.protobuf.empty_pb2 as empty

from tts_pb2_grpc import AcousticStub
from tts_pb2 import InputText, Model


class AcousticClient(object):
    def __init__(self, remote='127.0.0.1:30001'):
        channel = grpc.insecure_channel(remote)
        self.stub = AcousticStub(channel)

    def txt2mel(self, speaker, text):
        return self.stub.Txt2Mel(InputText(speaker=speaker, text=text))

    def stream_txt2mel(self, speaker, text):
        return self.stub.StreamTxt2Mel(InputText(speaker=speaker, text=text))

    def get_mel_config(self):
        return self.stub.GetMelConfig(empty.Empty())

    def set_model(self, checkpoint_path, threshold=0.5, max_decoder_steps=1200):
        in_model = Model(
            path=checkpoint_path,
            config={
                "threshold": str(threshold),
                "max_decoder_steps": str(max_decoder_steps)
            }
        )
        return self.stub.SetModel(in_model)

    def get_model(self):
        return self.stub.GetModel(empty.Empty())


if __name__ == '__main__':
    client = AcousticClient()

    mel_config = client.get_mel_config()
    print(mel_config)

    mel = client.txt2mel(0, '나는 사과를 먹는다.')
    print(mel)

    for mel in client.stream_txt2mel(0, '나는 사과를 먹는다.'):
        print(mel)

    model = client.get_model()
    print(model)
