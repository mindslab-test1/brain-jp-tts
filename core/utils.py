import os
import grpc
import time
import logging
import numpy as np
from scipy.io.wavfile import read
import torch

logger = logging.getLogger('tacotron')


def enum(**enums):
    return type('Enum', (), enums)

speaker_mode = enum(NONE=0, ENTITY_EMBEDDING=1, SPEAKER_ENCODING=2)

def get_mask_from_lengths(lengths):
    max_len = torch.max(lengths).item()
    ids = torch.arange(0, max_len, out=torch.cuda.LongTensor(max_len))
    mask = (ids < lengths.unsqueeze(1)).bool()
    return mask


def load_wav_to_torch(full_path, sr):
    sampling_rate, data = read(full_path)
    if len(data.shape) == 2:
        data = data[:, 0]
    if data.dtype != np.int16:
        raise RuntimeError('{} is not int16'.format(full_path))
    assert sr == sampling_rate, "{} SR doesn't match {} on path {}".format(
        sr, sampling_rate, full_path)
    return torch.FloatTensor(data.astype(np.float32))


def load_filepaths_and_text(filename, sort_by_length, split="|"):
    with open(filename, encoding='utf-8') as f:
        filepaths_and_text = [line.strip().split(split) for line in f]

    if sort_by_length:
        filepaths_and_text.sort(key=lambda x: len(x[1]))

    return filepaths_and_text


def to_gpu(x):
    x = x.contiguous().cuda(non_blocking=True)
    return torch.autograd.Variable(x)


def set_logger(level):
    os.environ['TZ'] = 'Asia/Seoul'
    try:
        time.tzset()
    except AttributeError as e:
        print(e)
        print("Skipping timezone setting.")
    custom_logger(name='tacotron')
    logger = logging.getLogger('tacotron')
    if level == 'DEBUG':
        logger.setLevel(logging.DEBUG)
    elif level == 'INFO':
        logger.setLevel(logging.INFO)
    elif level == 'WARNING':
        logger.setLevel(logging.WARNING)
    elif level == 'ERROR':
        logger.setLevel(logging.ERROR)
    elif level == 'CRITICAL':
        logger.setLevel(logging.CRITICAL)
    return logger


def custom_logger(name):
    fmt = '[{}|%(levelname)s|%(filename)s:%(lineno)s] %(asctime)s >>> %(message)s'.format(name)
    fmt_date = '%Y-%m-%d_%T %Z'

    handler = logging.StreamHandler()

    formatter = logging.Formatter(fmt, fmt_date)
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(handler)


def log_error(func):
    def wrapper(*args, **kwargs):
        try:
            logger.debug('start %s', func)
            return func(*args, **kwargs)
        except Exception as e:
            logger.exception(e)
            context = args[2]
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))
        finally:
            logger.debug('end %s', func)
    return wrapper
