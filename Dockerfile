FROM nvcr.io/nvidia/pytorch:19.06-py3

RUN python3 -m pip uninstall -y apex && \
    python3 -m pip --no-cache-dir install --upgrade \
        torch==1.5.1+cu101 \
        torchvision==0.6.1+cu101 \
        -f https://download.pytorch.org/whl/torch_stable.html && \
    /opt/conda/bin/conda install openblas!=0.3.6 && \
    cd /tmp && \
    git clone https://github.com/NVIDIA/apex && \
    cd apex && \
    python3 -m pip install -v --no-cache-dir \
        --global-option="--pyprof" \
        --global-option="--cpp_ext" \
        --global-option="--cuda_ext" \
        --global-option="--bnp" \
        --global-option="--xentropy" \
        --global-option="--deprecated_fused_adam" ./ && \
# ==================================================================
# config & cleanup
# ------------------------------------------------------------------
    ldconfig && \
    apt-get clean && \
    apt-get autoremove && \
    rm -rf /var/lib/apt/lists/* /tmp/* /workspace/*

RUN python3 -m pip --no-cache-dir install --upgrade \
        tensorflow==1.9.0 \
        tensorboardX==1.2 \
        grpcio==1.13.0 \
        grpcio-tools==1.13.0 \
        protobuf==3.5.1 \
        xpinyin==0.5.6

RUN mkdir /root/tacotron2

COPY . /root/tacotron2

RUN cd /root/tacotron2 &&\
    python -m grpc.tools.protoc \
        --proto_path=tts/proto/src/main/proto/maum/brain/tts \
        --python_out=. \
        --grpc_python_out=. \
        tts/proto/src/main/proto/maum/brain/tts/tts.proto